![polypacket_logo](https://gitlab.com/uprev/public/mrt/tools/polypacket/-/raw/master/logo.png)

Poly Packet is a set of tools aimed at generating protocols from embedded projects. Protocols are described in an YAML document which can be easily shared with all components of a system.

A python script is used to parse the YAML file and generate C/C++ code as well as documentation. The code generation tool can create the back end service, application layer, and even an entire linux utility app


For more information see the documentation page [PolyPacket](https://mrt.readthedocs.io/en/latest/pages/polypacket/polypacket.html)